import { MouseEventHandler } from "react";

export interface customButtonProps{
  title:string,
  containerStyles?: string,
  handleClick?: MouseEventHandler<HTMLInputElement>,
  btnType?: 'button' | 'submit'
}

export interface searchManufacturerProps{
  manufacturer:string,
  setManufacturer: (manufacturer:string) => void
}