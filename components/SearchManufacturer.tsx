import { searchManufacturerProps } from '@/types'
import { Combobox, Transition } from '@headlessui/react'
import Image from 'next/image'

const SearchManufacturer = ({manufacturer, setManufacturer}:searchManufacturerProps) => {
  const [query, setQuery] = useState('')
  return (
    <div className="search-manufacture">
      <Combobox>
        <div className="relative w-full">
          <Combobox.Button className="absolute top-[14px]">
            <Image
            src="/car-logo.svg"
            alt="asd"
            width={20}
            height={20}
            className="ml-4"
            />
          </Combobox.Button>
          <Combobox.Input
            className="search-manufacture__input"
            placeholder="Volkswagen"
            displayValue={(manufacturer:string)=>manufacturer}
            onChange={(e=>setQuery(e.target.value))}
          />
        </div>14px
      </Combobox>
    </div>
  )
}

export default SearchManufacturer