'use client'

import { customButtonProps } from "@/types"

const CustomButton = ({btnType,title, containerStyles, handleClick}:customButtonProps) => {
    return (
        <button
            className={`custom-btn ${containerStyles}`}
            type={btnType || 'button'}
            disabled={false}
            onClick={handleClick}
        >
            <span className={`flex-1`} >{title}</span>
        </button>
    )
}

export default CustomButton;